package InterfaceLayer;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

import LogicLayer.Game;
import LogicLayer.GameBoard;
import Utilities.RandomEngine;

public class GraphicGame extends Canvas implements Runnable, KeyListener {
	private static final long serialVersionUID = -8225321282693611951L;

	public static final int WIDTH = 800, HEIGHT = WIDTH / 12 * 9;
	
	private Thread thread;
	private boolean running = false;
	private Handler handler;
	Graphics graficos;
	Game game;

	public GraphicGame() {

		game = new Game();
		handler = new Handler();

		new Window(WIDTH, HEIGHT, "2048 game", this);
		handler.addObject(new Player(500, 500, ID.Player,game));
	}

	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
		requestFocusInWindow();
	}

	public synchronized void stop() {
		try {
			thread.join();
			running = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		this.addKeyListener(this);
		
		RandomEngine random = new RandomEngine();
		game.getGB().autoGenerateNumber(random);
		game.getGB().autoGenerateNumber(random);
		
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;

			
			while (delta >= 1) {
				tick();
				delta--;
			}
			if (running){
				render();
			}
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
			}
			game.cleanFlag();
			if(game.gameOver()){
				System.out.println("Juego terminado");
				//message(g, false);
				break;
			}
			
			if(game.wonGame(2048)){	
								
				System.out.println("Ganaste");
				//message(g, false);
				break;
			}
		}
		render();
		stop();

	}

	private void tick() {
		handler.tick();
	}

	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
		Color celeste = new Color(184,219,253);
		g.setColor(celeste);
		g.fillRect(0, 0, WIDTH, HEIGHT);

		handler.render(g);

		g.dispose();
		bs.show();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		RandomEngine random = new RandomEngine();
		GameBoard check = new GameBoard(4);
		check = game.getGB().clone();
		

		if(key == KeyEvent.VK_UP)
		{
			game.moveLeft();
			if(!check.equals(game.getGB()))
				game.getGB().autoGenerateNumber(random);
		}

		if(key == KeyEvent.VK_DOWN)
		{
			game.moveRight();
			if(!check.equals(game.getGB()))
				game.getGB().autoGenerateNumber(random);
		}

		if(key == KeyEvent.VK_LEFT)
		{
			game.moveUp();
			if(!check.equals(game.getGB()))
				game.getGB().autoGenerateNumber(random);
		}

		if(key == KeyEvent.VK_RIGHT){
			game.moveDown();
			if(!check.equals(game.getGB()))
				game.getGB().autoGenerateNumber(random);
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {	}

	@Override
	public void keyTyped(KeyEvent e) {}

	public static void main(String args[]) {
		new GraphicGame();
	}

	public void message(Graphics g, boolean win){
		Font font=new Font("ComicSansMS", Font.PLAIN, 40);
		g.setFont(font);
		
		if (win)
			g.drawString("Ganaste!", 200, 300);
		else
			g.drawString("Game Over!", 200, 300);
	}
}
