package InterfaceLayer;

import java.awt.Color;
import java.awt.Graphics;

public class Square {
	
	int x,y;
	int size;
	Graphics g;
	
	public Square(int ax,int ay, int asize)
	{
		x = ax;
		y = ay;
		size = asize;
	}
	
	public void draw(Graphics g, int x, int y, int value){

		Color squareColor = setSquareColor((value)); 
		g.setColor(squareColor);
		g.fillRect(x, y , size, size);

	}
	
	public Color setSquareColor(int value){


		Color color2048 = new Color(230,190,65);
		Color color128 = new Color(235,205,120);
		Color color64 = new Color(230,110,75);
		Color color32 = new Color(230,130,105);
		Color color16 = new Color(235,140,85);
		Color color8 = new Color(230,180, 130);
		Color color4 = new Color(235,225,200);
		Color color2 = new Color(235,230,219);
		
		if (value == 2) return color2;
		if (value == 4) return color4;
		if (value == 8) return color8;
		if (value == 16) return color16;
		if (value == 32) return color32;
		if (value == 64) return color64;
		if (value >= 128) return color128;
		if (value == 2048) return color2048;
		if (value <= 8192) return Color.darkGray;
		if (value <= 32768) return Color.magenta.darker();
		
		
		return Color.white;
		
	}
	
}
