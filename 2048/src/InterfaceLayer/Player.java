package InterfaceLayer;

import java.awt.Color;
import java.awt.Graphics;

import LogicLayer.Game;

public class Player extends GameObject {
	Game game;
	public Player(int x, int y, ID id, Game ngame) {
		super(x, y, id);
		game = ngame;
	}

	public void tick() {
		x += moveX;
		y += moveY;
	}

	public void render(Graphics graficos) {

		Color fondo = new Color(170,160,160);
		graficos.setColor(fondo);
		graficos.fillRect(130, 50, 500, 485);
		
		dibujarMatrizFondo(graficos);
		dibujarMatriz(graficos);
	}

	public void dibujarMatrizFondo(Graphics graficos) {
		Color matriz = new Color(200,190,180);
		graficos.setColor(matriz);
		int xInicial = 160;
		int yInicial = 75;
		int distX = 115;
		int distY = 115;

		for (int coordX = 0; coordX < 4; coordX++) {
			for (int coordY = 0; coordY < 4; coordY++) {
				graficos.fillRect(xInicial + distX * coordX, yInicial + distY * coordY, 90, 90);
			}
		}
	}

	public void dibujarMatriz(Graphics graficos) {

		int xInicial = 160 + 15;
		int yInicial = 75 + 15;
		int dist = 115;
		Square square = new Square(1, 1, 90);

		for (int coordX = 0; coordX < 4; coordX++) {
			for (int coordY = 0; coordY < 4; coordY++) {
					if (game.getGB().getNumber(coordX, coordY) != null){
					 
					Numero n = new Numero(xInicial + dist * coordX, yInicial + dist * coordY,game.getGB().getNumber(coordX, coordY).getValue() );
					
					graficos.setColor(square.setSquareColor((n.value)));
		
					graficos.fillRect(xInicial + dist * coordX, yInicial + dist * coordY, 90, 90);
	
					square.draw(graficos, xInicial + dist * coordX, yInicial + dist * coordY, n.value);
					graficos.setColor(square.setSquareColor((n.value)));
	
					n.draw(graficos);
				}
			}
		}

	}

}
