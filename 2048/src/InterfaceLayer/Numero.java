package InterfaceLayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Numero {
	int x,y;
	Integer value;
	Graphics g;
	
	public Numero(int x, int y, int value)
	{
		this.x=x;
		this.y=y;
		this.value=value;
	}
	
	public void draw(Graphics g){
		String string = value.toString();
		
		int size = fontSize(); 
		int a = alignX();
		int b = alignY();
		
		Font font=new Font("ComicSansMS", Font.BOLD, size);
		g.setFont(font);
		
		
		if (value >= 8)
			g.setColor(Color.white);
		else
			g.setColor(Color.black);
				
		
		g.drawString(string, x+a, y+b); 
	}
	
	public int alignX(){
		
		if (value < 10) return 30;
		if (value < 100) return 14;
		if (value < 1000) return 8;
		if (value < 10000) return 8;
		if (value < 100000) return 10;
		
		return 0;
	}
	
	public int alignY(){
		
		if (value < 100) return 65;
		if (value < 1000) return 62;
		if (value < 10000) return 58;
		if (value < 100000) return 52;
		
		return 0;
	}

	public int fontSize(){

		if (value < 100) return 55;
		if (value < 1000) return 45;
		if (value < 10000) return 35;
		if (value < 100000) return 25;
	
		return 0;
	}

}

