package Utilities;

import java.util.Random;

public class RandomEngine {

	public int getRandom(int n){
		int ret = 0;
		Random ran = new Random();
		ret = ran.nextInt(n);
		return ret;
	}

	public int getFakeRandom(int i) {
		return i;
	}
	
}
