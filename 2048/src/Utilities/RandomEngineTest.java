package Utilities;

import static org.junit.Assert.*;

import org.junit.Test;

public class RandomEngineTest {

	@Test
	public void getRandom() {
			RandomEngine random = new RandomEngine();
			
			assertTrue( 4 == random.getFakeRandom(4));
			assertFalse(0 == random.getFakeRandom(3));
	}

}
