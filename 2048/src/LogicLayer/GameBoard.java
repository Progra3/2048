package LogicLayer;

import Utilities.Direction;
import Utilities.RandomEngine;

public class GameBoard {

	Number[][] _gameBoard;
	

	public GameBoard(int n) {
		if (n < 4)
			throw new RuntimeException("Illegal number of Game Board: " + n);

		_gameBoard = new Number[n][n];
		
	}

	public Number getNumber(int x, int y) {
		argumentException(x, y);

		return _gameBoard[x][y];
	}

	public void setNumberOnGB(int value, int x, int y) {
		argumentException(x, y);
		if (value == 0)
			_gameBoard[x][y] = null;
		else {
			Number added = new Number(value, x, y);
			_gameBoard[x][y] = added;
		}
	}

	public int lenght(){
		return _gameBoard.length;
	}
	
	public void autoGenerateNumber(RandomEngine random) {
		// at random position generates random Number's.
		int x = random.getRandom(4);
		int y = random.getRandom(4);

		while (doesNumberExistOnGB(x, y)) {
			x = random.getRandom(4);
			y = random.getRandom(4);
		}

		setAleatoryNumberOnGB(new Number(x, y));

	}

	public boolean numberNextToBound(Direction dir, Number x) { // return true
																// if Number is
																// next to
																// bound.
		if (x == null)
			return false;

		switch (dir) {
		case UP:
			return x.getX() == 0 || getNumber(x.getX() - 1,x.getY()) != null;
		case DOWN:
			return x.getX() == _gameBoard.length - 1 || getNumber(x.getX() + 1, x.getY()) != null;
		case LEFT:
			return x.getY() == 0 || getNumber(x.getX(), x.getY() - 1) != null;
		case RIGHT:
			return x.getY() == _gameBoard.length - 1 || getNumber(x.getX(), x.getY() + 1) != null;
		default:
			return false;
		}
	} 

	public void moveNumber(Direction dir, Number x) {
		switch (dir) {
		case UP:
			if (!(numberNextToBound(Direction.UP, x))) {
				setNumberOnGB(x.getValue(), x.getX() - 1, x.getY());
				setNumberOnGB(0, x.getX(), x.getY());
			}
			break;

		case DOWN:
			if (!(numberNextToBound(Direction.DOWN, x))) {
				setNumberOnGB(x.getValue(), x.getX() + 1, x.getY());
				setNumberOnGB(0, x.getX(), x.getY());
			}
			break;
		case LEFT:
			if (!(numberNextToBound(Direction.LEFT, x)) ) {
				setNumberOnGB(x.getValue(), x.getX(), x.getY() - 1);
				setNumberOnGB(0, x.getX(), x.getY());
			}
			break;
		case RIGHT:
			if (!(numberNextToBound(Direction.RIGHT, x))) {
				setNumberOnGB(x.getValue(), x.getX(), x.getY() + 1);
				setNumberOnGB(0, x.getX(), x.getY());
			}
			break;
		default:
			throw new RuntimeException("Could not move Number: " + x.getValue() + "at position " + x.getX() + " " + x.getY());

		}
	}

	public void addNumber(Number number, Number number2) {
		if (number.flag ==true || number2.flag ==true)
			return;
		
		number2.addNumber(number);
		number2.flag = true;
		setNumberOnGB(0, number.getX(), number.getY());
	}

	public boolean isFull(){
		boolean ret = true;
		for(int i = 0; i < lenght() ; i++){
			for(int j = 0; j < lenght() ; j++){
				ret &= _gameBoard[i][j] != null;
			}
		}
		return ret;
	}

	public boolean differentNumbers(){
		boolean ret = false;
		for(int i = 0; i < lenght() ; i++){
			for(int j = 0; j < lenght() -1; j++){
				if(_gameBoard[i][j] != null)
				ret &= _gameBoard[i][j].equals(_gameBoard[i][j+1]);
			}
		}
		for(int j = 0; j < lenght() ; j++){
			for(int i = 0; i < lenght() -1; i++){
				if(_gameBoard[i][j] != null)
				ret &= _gameBoard[i][j].equals(_gameBoard[i + 1][j]);
			}
		}
		return ret;
	}
	
	public String toString() {

		String ret = "";
		for (int i = 0; i < _gameBoard.length; i++) {
			ret += "\n";
			for (int j = 0; j < _gameBoard.length; j++) {
				if (_gameBoard[i][j] == null)
					ret += "-";
				else
					ret += _gameBoard[i][j].toString();
			}
		}
		return ret;
	}

	public boolean equals(Object x){
		boolean ret = true;
		
		if (x == null)
				return false;			
		if (this.getClass() != x.getClass())
				return false;
		
		for (int i = 0; i < (((GameBoard)x)._gameBoard).length; i++){
			for (int j = 0; j < (((GameBoard)x)._gameBoard).length; j++)
				if (getNumber(i,j) == null && ((GameBoard)x).getNumber(i, j) == null)
					ret &= true;
				else if(getNumber(i,j) == null && ((GameBoard)x).getNumber(i, j) != null)
					ret &= false;
				else	
					ret &= getNumber(i,j).equals(((GameBoard)x).getNumber(i, j));
		}	
		return ret;
	}

	void argumentException(int x, int y) {
		if (x > _gameBoard.length - 1 || y > _gameBoard.length - 1  || x < 0 || y < 0)
			throw new IllegalArgumentException("Wrong values: " + x + " " + y);
	}

	boolean doesNumberExistOnGB(int x, int y) {
		return !(_gameBoard[x][y] == null);
	}
	
	public boolean doesSpecificNumberExistOnGB(int value) {
		
		Number wanted = new Number(value);
		boolean ret = false;
				
		for (int i = 0; i < _gameBoard.length; i++) {
			for (int j = 0; j < _gameBoard.length; j++){
				if (_gameBoard[i][j] != null)
					ret |= _gameBoard[i][j].equals(wanted); 
				
			}
		}
		
		return ret;
	}
	void setAleatoryNumberOnGB(Number number) {
		_gameBoard[number.getX()][number.getY()] = number;
	}

	public int numberPosition(int x, int y) {
		return _gameBoard[x][y].getValue();
	}
	
	public GameBoard clone(){
		GameBoard board = new GameBoard(4);
		int value = 0;
		for (int i = 0; i < _gameBoard.length; i++) {
			for (int j = 0; j < _gameBoard.length; j++){
				if (_gameBoard[i][j] != null){
					value = _gameBoard[i][j].getValue();
					board.setNumberOnGB(value, i, j);
				}	
			}
		}
		return board;
	}
	
}
