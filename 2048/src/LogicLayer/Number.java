package LogicLayer;

import java.util.Random;

public class Number {
	private int value;
	 boolean flag;
	private int posX;
	private int posY;

	Number(int x, int y){/* this method is called when the user moves the table.*/
								/*It generates randomly Numbers with value 2 or 4 */
	Random random = new Random(); //Pasar RandomEngine
		posX = x;
		posY = y;
	
		int sw = random.nextInt(2);
		
		switch(sw){
			case 0: value = 2; break;
			case 1: value = 4; break;
			default:
				throw new RuntimeException("Imposible to create new Number");
		}	
	} 

	Number (int val, int x, int y){
		value = val;
		posX = x;
		posY = y;
	}
	
	Number (int val){
		value = val;
	}
							
	public int getX(){
		return posX;
	}
	
	public int getY(){
		return posY;
	}
	
	public int getValue(){
		return value;
	}
	
	public void turnOffFlag(){
		flag = false;
	}
	
	public void addNumber(Number x){
		value = value + x.value;
	}
	
	public boolean equals(Object x){

		if (x == null)
			return false;
		
		if (this.getClass() != x.getClass())
			return false;
		
		if(value == ((Number)x).value )
			return true;
		
		else 
			return false;
	}

	public String toString(){
		String ret = "";
		ret ="" + value;
		return ret;
	}

}
