package LogicLayer;

import Utilities.Direction;
import Utilities.RandomEngine;

public class Game {
	private GameBoard _gameBoard;
	
	public Game(){
		_gameBoard = new GameBoard(4);
	}
		
	public GameBoard getGB(){
		return _gameBoard;
	}
	
	public void setGB(GameBoard x){
		_gameBoard =  x;
	}
	
	public void initGame(){
		RandomEngine random = new RandomEngine();
		_gameBoard.autoGenerateNumber(random);
		_gameBoard.autoGenerateNumber(random);
	}
	
	public void moveRight() {
		for (int i = 0; i < _gameBoard.lenght(); i++){
			for (int j = _gameBoard.lenght() -2; j >= 0; j--){
				if (_gameBoard.getNumber(i,j) != null){
					int aux = j;
					while(!_gameBoard.numberNextToBound(Direction.RIGHT,_gameBoard.getNumber(i,aux))){
						_gameBoard.moveNumber(Direction.RIGHT, _gameBoard.getNumber(i,aux));
						aux ++;
					}
					if (aux != 3){
						if(_gameBoard.getNumber(i,aux).equals(_gameBoard.getNumber(i,aux+1)))
							_gameBoard.addNumber(_gameBoard.getNumber(i,aux), _gameBoard.getNumber(i,aux+1));
					}
				}
			}
		}		
	} 

	public void moveLeft(){
		for (int i = 0; i < _gameBoard.lenght(); i++){
			for (int j = 1; j < _gameBoard.lenght(); j++){
				if (_gameBoard.getNumber(i,j) != null){
					int aux = j;
					while(!_gameBoard.numberNextToBound(Direction.LEFT,_gameBoard.getNumber(i,aux))){
						_gameBoard.moveNumber(Direction.LEFT, _gameBoard.getNumber(i,aux));
						aux --;
					}
					if (aux != 0){
						if(_gameBoard.getNumber(i,aux).equals(_gameBoard.getNumber(i,aux-1)))
							_gameBoard.addNumber(_gameBoard.getNumber(i,aux), _gameBoard.getNumber(i,aux-1));
					}
				}
			}
		}		
	} 
		
	public void moveDown(){
		for (int j = 0; j < _gameBoard.lenght(); j++){
			for (int i = _gameBoard.lenght() -2; i >= 0; i--){
				if (_gameBoard.getNumber(i,j) != null){
					int aux = i;
					while(!_gameBoard.numberNextToBound(Direction.DOWN,_gameBoard.getNumber(aux,j))){
						_gameBoard.moveNumber(Direction.DOWN, _gameBoard.getNumber(aux,j));
						aux ++;
					}
					if (aux != 3){
						if(_gameBoard.getNumber(aux,j).equals(_gameBoard.getNumber(aux+1, j)))
							_gameBoard.addNumber(_gameBoard.getNumber(aux,j), _gameBoard.getNumber(aux+1,j));
					}
				}
			}
		}		
	} 
	
	public void moveUp(){
		for (int j = 0; j < _gameBoard.lenght(); j++){
			for (int i = 1; i < _gameBoard.lenght(); i++){
				if (_gameBoard.getNumber(i,j) != null){
					int aux = i;
					while(!_gameBoard.numberNextToBound(Direction.UP,_gameBoard.getNumber(aux,j))){
						_gameBoard.moveNumber(Direction.UP, _gameBoard.getNumber(aux,j));
						aux --;
					}
					if (aux != 0){
						if(_gameBoard.getNumber(aux,j).equals(_gameBoard.getNumber(aux -1,j)))
							_gameBoard.addNumber(_gameBoard.getNumber(aux,j), _gameBoard.getNumber(aux -1,j));
					}
				}
			}
		}	
	}

	public void cleanFlag(){
		for(int i = 0; i < _gameBoard.lenght(); i++){
			for(int j = 0; j < _gameBoard.lenght(); j++){
				if(_gameBoard.getNumber(i, j) != null)
					_gameBoard.getNumber(i, j).turnOffFlag();
			}
		}
	}
	
	public boolean gameOver(){
		return _gameBoard.isFull() && _gameBoard.differentNumbers(); 
	}
	
	public boolean wonGame(int target){
		return _gameBoard.doesSpecificNumberExistOnGB(target); 
	}
	
	public int numberPosition(int x, int y){
		return _gameBoard.numberPosition(x,y);
	}
	
	public String toString(){
		return _gameBoard.toString();
	}

	public boolean equals(Object x){
		if (x == null)
				return false;			
		if (this.getClass() != x.getClass())
				return false;
		
		return _gameBoard.equals(((Game)x).getGB());
	}

	
	
}