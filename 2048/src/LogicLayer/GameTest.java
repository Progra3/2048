package LogicLayer;
import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void initGameTest() {
	}

	@Test
	public void equalsTest(){
		Game game = instanceGame();
		Game game2 = instanceGame2();
		assertEquals(game, game);
		assertNotEquals(game, game2);
	}
	
	@Test
	public void moveRightTest(){
		Game game = instanceGame();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(64,0,3);
		board.setNumberOnGB(2,0,2);
		board.setNumberOnGB(4,1,3);
		board.setNumberOnGB(4,1, 2);
		board.setNumberOnGB(8,2,3);
		board.setNumberOnGB(2,2, 2);
		board.setNumberOnGB(2,3, 3);
		
		Game game2 = instanceGame3();
		GameBoard board2 = new GameBoard(4);
		board2.setNumberOnGB(2,0,2);
		board2.setNumberOnGB(4,0,3);
		board2.setNumberOnGB(2,1,2);
		board2.setNumberOnGB(4,1,3);
		board2.setNumberOnGB(4,2,2);
		board2.setNumberOnGB(4,2,3);
		board2.setNumberOnGB(4,3,2);
		board2.setNumberOnGB(4,3,3);
		
		game.moveRight();
		game2.moveRight();
		
		assertEquals(game.getGB(), board);
		assertEquals(game2.getGB(), board2);
	}
	
	@Test
	public void moveDownTest(){
		Game game = instanceGame();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(4,2,0);
		board.setNumberOnGB(32,2,2);
		board.setNumberOnGB(32,2,3);
		board.setNumberOnGB(4,3,0);
		board.setNumberOnGB(2,3,1);
		board.setNumberOnGB(8,3,2);
		board.setNumberOnGB(4,3,3);
		
		Game game2 = instanceGame3();
		GameBoard board2 = new GameBoard(4);
		board2.setNumberOnGB(4,2,0);
		board2.setNumberOnGB(4,3,0);
		board2.setNumberOnGB(2,2,1);
		board2.setNumberOnGB(4,3,1);
		board2.setNumberOnGB(2,2,2);
		board2.setNumberOnGB(4,2,3);
		board2.setNumberOnGB(4,3,2);
		board2.setNumberOnGB(4,3,3);
		
		game.moveDown();
		game2.moveDown();
		
		assertEquals(game.getGB(), board);
		assertEquals(game2.getGB(), board2);
		
		}
	
	@Test
	public void moveUpTest(){
		Game game = instanceGame();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(4,0,0);
		board.setNumberOnGB(2,0,1);
		board.setNumberOnGB(32,0,2);
		board.setNumberOnGB(32,0,3);
		board.setNumberOnGB(4,1,0);
		board.setNumberOnGB(8,1,2);
		board.setNumberOnGB(4,1,3);
		
		Game game2 = instanceGame3();
		GameBoard board2 = new GameBoard(4);
		board2.setNumberOnGB(4,0,0);
		board2.setNumberOnGB(4,1,0);
		board2.setNumberOnGB(4,0,1);
		board2.setNumberOnGB(2,1,1);
		board2.setNumberOnGB(4,0,2);
		board2.setNumberOnGB(2,1,2);
		board2.setNumberOnGB(4,0,3);
		board2.setNumberOnGB(4,1,3);
		
		game.moveUp();
		game2.moveUp();
		assertEquals(game.getGB(), board);
		assertEquals(game2.getGB(), board2);
		}
	
	@Test
	public void moveLeftTest(){
		Game game = instanceGame();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(2,0,0);
		board.setNumberOnGB(64,0,1);
		board.setNumberOnGB(4,1,0);
		board.setNumberOnGB(4,1, 1);
		board.setNumberOnGB(2,2,0);
		board.setNumberOnGB(8,2, 1);
		board.setNumberOnGB(2,3,0);
		
		Game game2 = instanceGame3();
		GameBoard board2 = new GameBoard(4);
		board2.setNumberOnGB(4,0,0);
		board2.setNumberOnGB(2,0,1);
		board2.setNumberOnGB(4,1,0);
		board2.setNumberOnGB(2,1,1);
		board2.setNumberOnGB(4,2,0);
		board2.setNumberOnGB(4,2,1);
		board2.setNumberOnGB(4,3,0);
		board2.setNumberOnGB(4,3,1);
		
		
		game.moveLeft();
		game2.moveLeft();
		assertEquals(game.getGB(), board);
		assertEquals(game2.getGB(), board2);
		}
	
 	private Game instanceGame(){
		Game ret = new Game();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(2,0,0);
		board.setNumberOnGB(2,1,0);
		board.setNumberOnGB(2,2,0);
		board.setNumberOnGB(2,3,0);
		board.setNumberOnGB(2,1, 1);
		board.setNumberOnGB(4,1, 2);
		board.setNumberOnGB(32,0,2);
		board.setNumberOnGB(32,0,3);
		board.setNumberOnGB(4, 2, 2);
		board.setNumberOnGB(4, 2, 3);
		
		ret.setGB(board);
		return ret;
	}
	
	private Game instanceGame2(){
		Game ret = new Game();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(2,2,2);
		board.setNumberOnGB(2,2,0);
		board.setNumberOnGB(4,0,2);
		board.setNumberOnGB(8, 3, 2);
		board.setNumberOnGB(16, 0, 3);
		board.setNumberOnGB(4, 3, 3);
		
		ret.setGB(board);
		return ret;
	}
	
	private Game instanceGame3(){
		Game ret = new Game();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(2,0,0);
		board.setNumberOnGB(2,0,2);
		board.setNumberOnGB(2,0,3);
		board.setNumberOnGB(2,1,0);
		board.setNumberOnGB(2,1,1);
		board.setNumberOnGB(2,1,3);
		board.setNumberOnGB(2,2,0);
		board.setNumberOnGB(2,2,1);
		board.setNumberOnGB(2,2,2);
		board.setNumberOnGB(2,2,3);
		board.setNumberOnGB(2,3,0);
		board.setNumberOnGB(2,3,1);
		board.setNumberOnGB(2,3,2);
		board.setNumberOnGB(2,3,3);
		
		ret.setGB(board);
		return ret;
	}
}
