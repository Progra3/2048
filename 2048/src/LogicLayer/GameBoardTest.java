package LogicLayer;

import static org.junit.Assert.*;
import org.junit.Test;

import Utilities.Direction;


public class GameBoardTest {

	@Test
	public void nextToBoundsTest() {
		GameBoard board = instanceGameBoard();
	
		assertFalse(board.numberNextToBound(Direction.RIGHT, board.getNumber(2,2)));//no next to bounds case
		assertTrue(board.numberNextToBound(Direction.RIGHT, board.getNumber(3,2))); //null case
		assertFalse(board.numberNextToBound(Direction.RIGHT,board. getNumber(2,0)));
		assertFalse(board.numberNextToBound(Direction.DOWN, board.getNumber(0,2)));
		assertTrue(board.numberNextToBound(Direction.UP, board.getNumber(0,2)));// next to bound case
		assertTrue(board.numberNextToBound(Direction.RIGHT, board.getNumber(0,3)));
		assertTrue(board.numberNextToBound(Direction.LEFT, board.getNumber(2,0)));
		assertTrue(board.numberNextToBound(Direction.DOWN, board.getNumber(3,2)));
		assertTrue(board.numberNextToBound(Direction.RIGHT, board.getNumber(3,2)));
		assertTrue(board.numberNextToBound(Direction.DOWN, board.getNumber(2,2)));
		assertTrue(board.numberNextToBound(Direction.UP, board.getNumber(3,2)));
		assertFalse(board.numberNextToBound(Direction.LEFT, board.getNumber(3,2)));
	}

	@Test
	public void lenghtTest(){
		GameBoard board = instanceGameBoard();
		assertTrue(board.lenght() == 4);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void argumentExceptionTest(){
		GameBoard board = instanceGameBoard();
		board.getNumber(7, 7);
		
	}
	
	@Test
	public void  autoGenerateNumberTest(){
	}
	
	@Test
	public void moveNumberTest(){
		GameBoard board = instanceGameBoard();
		Number x1 = new  Number(2,1,2);
		Number x2 = new  Number(2,2,1);
		Number x3 = new  Number(4,0,1);
		Number x4 = new  Number(4,3,3);
		Number x5 = new  Number(8,3,2);
		Number x6 = new  Number(16,1,3);
		
		System.out.println(board.toString());
		board.moveNumber(Direction.DOWN, board.getNumber(0, 3));
		board.moveNumber(Direction.UP, board.getNumber(2, 2));
		board.moveNumber(Direction.LEFT, board.getNumber(0, 2));
		board.moveNumber(Direction.RIGHT, board.getNumber(2, 0));
		board.moveNumber(Direction.LEFT, board.getNumber(3, 3));
		System.out.println(board.toString());
		
		assertEquals(x1, board.getNumber(1, 2));
		assertEquals(x2, board.getNumber(2, 1));
		assertEquals(x3, board.getNumber(0, 1));
		assertEquals(x4, board.getNumber(3, 3));
		assertEquals(x5, board.getNumber(3, 2));
		assertEquals(x6, board.getNumber(1, 3));
		
	}
	
	@Test
	public void getNumberTest(){
		GameBoard board = instanceGameBoard();

		assertEquals(board._gameBoard[2][2], board.getNumber(2, 2));
		assertEquals(board._gameBoard[2][0], board.getNumber(2, 0));
		assertEquals(board._gameBoard[0][3], board.getNumber(0, 3));
		assertNotEquals(board._gameBoard[2][2], board.getNumber(1, 2));	
	}
	
	@Test
	public void doesNumberExistOnGBTest(){
		GameBoard board = instanceGameBoard();
			
		assertTrue(board.doesNumberExistOnGB(2, 2));
		assertTrue(board.doesNumberExistOnGB(3, 2));
		assertFalse(board.doesNumberExistOnGB(0, 0));
		assertFalse(board.doesNumberExistOnGB(1, 2));
	}

	@Test
	public void addNumbersTest(){
		GameBoard board = instanceGameBoard2();
		Number x1 = new Number(4,1,0);
		Number x2 = new Number(4,3,0);
		Number x3 = new Number(64,0,3);
		Number x4 = new Number(8,2,2);
		
		board.addNumber(board.getNumber(0, 0), board.getNumber(1, 0));
		board.addNumber(board.getNumber(2, 0), board.getNumber(3, 0));
		board.addNumber(board.getNumber(0, 2), board.getNumber(0, 3));
		board.addNumber(board.getNumber(2, 3), board.getNumber(2, 2));
	
		assertEquals(board.getNumber(1,0), x1);
		assertEquals(board.getNumber(3,0), x2);
		assertEquals(board.getNumber(0,3), x3);
		assertEquals(board.getNumber(2,2), x4);
		assertTrue(board.getNumber(0, 0) == null);
		assertTrue(board.getNumber(2, 0) == null);
		assertTrue(board.getNumber(0, 2) == null);
		assertTrue(board.getNumber(2, 3) == null);
		
		assertTrue(board.getNumber(1,0).flag);
		assertTrue(board.getNumber(3,0).flag);
		assertTrue(board.getNumber(0,3).flag);
		assertTrue(board.getNumber(2,2).flag);		
	}

	@Test
	public void isFullTest(){
		GameBoard gb1 = instanceGameBoard2();
		GameBoard gb2 = instance4();
		
		System.out.println(gb2.toString());
		assertFalse(gb1.isFull());
		assertTrue(gb2.isFull());
	}

	public void differentNumbers(){
		GameBoard gb2 = instance4();
		assertTrue(gb2.differentNumbers());
	}
	
	private GameBoard instance4() {
		GameBoard gb2 = new GameBoard(4);
		gb2.setNumberOnGB(16, 0, 0);
		gb2.setNumberOnGB(1, 1, 0);
		gb2.setNumberOnGB(2, 2, 0);
		gb2.setNumberOnGB(3, 3, 0);
		gb2.setNumberOnGB(4, 0, 1);
		gb2.setNumberOnGB(5, 1, 1);
		gb2.setNumberOnGB(6, 2, 1);
		gb2.setNumberOnGB(7, 3, 1);
		gb2.setNumberOnGB(8, 0, 2);
		gb2.setNumberOnGB(9, 1, 2);
		gb2.setNumberOnGB(10, 2, 2);
		gb2.setNumberOnGB(11, 3, 2);
		gb2.setNumberOnGB(12, 0, 3);
		gb2.setNumberOnGB(13, 1, 3);
		gb2.setNumberOnGB(14, 2, 3);
		gb2.setNumberOnGB(15, 3, 3);
		return gb2;
	}
	
	@Test
	public void equalsTest(){
		GameBoard g1 = instanceGameBoard2();
		GameBoard g2 = instanceGameBoard();
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(2,0,0);
		board.setNumberOnGB(2,1,0);
		board.setNumberOnGB(2,2,0);
		board.setNumberOnGB(2,3,0);
		board.setNumberOnGB(2,1, 1);
		board.setNumberOnGB(32,0,2);
		board.setNumberOnGB(32,0,3);
		board.setNumberOnGB(4, 2, 2);
		board.setNumberOnGB(4, 3, 3);
		
		assertEquals(g1, g1);
		assertNotEquals(g1, board);
		assertNotEquals(g1, g2);
		assertNotEquals(g1, null);
	}
	
	private GameBoard instanceGameBoard2() {
	GameBoard board = new GameBoard(4);
	board.setNumberOnGB(2,0,0);
	board.setNumberOnGB(2,1,0);
	board.setNumberOnGB(2,2,0);
	board.setNumberOnGB(2,3,0);
	board.setNumberOnGB(2,1, 1);
	board.setNumberOnGB(32,0,2);
	board.setNumberOnGB(32,0,3);
	board.setNumberOnGB(4, 2, 2);
	board.setNumberOnGB(4, 2, 3);
	
	return board;
	}

	private GameBoard instanceGameBoard() {
		GameBoard board = new GameBoard(4);
		board.setNumberOnGB(2,2,2);
		board.setNumberOnGB(2,2,0);
		board.setNumberOnGB(4,0,2);
		board.setNumberOnGB(8, 3, 2);
		board.setNumberOnGB(16, 0, 3);
		board.setNumberOnGB(4, 3, 3);
		
		return board;
	}

}

