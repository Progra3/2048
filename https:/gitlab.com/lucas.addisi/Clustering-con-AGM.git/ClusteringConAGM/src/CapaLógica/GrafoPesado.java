package CapaLógica;

public class GrafoPesado {
	private Integer [][] matrizDeAdyacencia;
	
	GrafoPesado(int n){
		 matrizDeAdyacencia = new Integer[n][n];
	}
	
	public int getArista(int i, int j){
		return matrizDeAdyacencia[i][j];
	}
	
	public void setArista(int i, int j, Integer peso){
		matrizDeAdyacencia[i][j] = matrizDeAdyacencia[j][i] = peso;
	}
	
	public int cantidadDeNodos(){
		return matrizDeAdyacencia.length;
	}
	
}
